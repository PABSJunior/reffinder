/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dados;

import beans.BeansResposta;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 *
 * @author Paulo Jr
 */
public class Processamento {
    
    public BeansResposta processa(String parametros){
        
        try {
            
            parametros = "data="+parametros;
            URL url = new URL("http://150.216.56.64/referencegene.php?type=reference");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            
            conn.setRequestMethod("POST");
            conn.setDoOutput(true);
            
            DataOutputStream saida = new DataOutputStream(conn.getOutputStream());
            saida.writeBytes(parametros);
            saida.flush();
            saida.close();
            
            int respostaCode = conn.getResponseCode();
            System.out.println("Response Code: "+respostaCode);
            System.out.println("Teste: "+conn.getResponseMessage());
            System.out.println("Resposta: ");
            
            BufferedReader leitor = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String linha;
            StringBuilder texto = new StringBuilder();
            
            while( (linha = leitor.readLine()) != null ){
                texto.append(linha);
                texto.append("\n");
            }
            
            String txt = texto.substring(texto.indexOf("<tr>\n" +
"        <td style=\"font:\"Times New Roman\", Times, serif\">"), texto.indexOf("<tr>\n" +
"        <td ><b> References </b> </td>"));
            
            Document html = Jsoup.parse(txt);
            
            BeansResposta resposta = new BeansResposta();
            
            //thead
            String stringInicio = "</b></td>", stringFim = "</tr>";
            int inicio = txt.indexOf(stringInicio)+stringInicio.length();
            int fim = txt.indexOf(stringFim)+stringFim.length();
            resposta.setThead(txt.substring(inicio, fim).replace("td>", "th>"));
            
            //tbody
            txt = txt.substring(fim);
            stringFim = "</table>";
            resposta.setTbody(txt.substring(0, txt.indexOf(stringFim)));
            
            //tabs
            Elements ul = html.select("ul").addClass("nav nav-tabs card-header-tabs");
            
            Elements li = ul.select("li");
            
            for( Element atual : li )//coloca class nos li
                atual.addClass("nav-item");
            
            Elements a = ul.select("a[href*='#tabs']");
            for( Element atual : a )
                atual.addClass("nav-link arrumaLink").attr("data-tabs", "");
            a.first().addClass("active").removeClass("arrumaLink").attr("style", "cursor: context-menu");
            
            String teste = "ul html = |||||"+ul.html()+"|||||\nul outerhtml = |||||"+ul.outerHtml()+"|||||\nul tostring = |||||"+ul.toString()+"|||||";
            
            String tab1 = html.select("#tabs-1").outerHtml();
            String tab2 = html.select("#tabs-2").addClass("d-none").outerHtml();
            String tab3 = html.select("#tabs-3").addClass("d-none").outerHtml();
            String tab4 = html.select("#tabs-4").addClass("d-none").outerHtml();
            String tab5 = html.select("#tabs-5").addClass("d-none").outerHtml();
            
            resposta.setTabs(ul.html());
//            resposta.setTabs(teste);
            resposta.setTabsconteudo(tab1+tab2+tab3+tab4+tab5);
            
            return resposta;
            
        } catch (MalformedURLException ex) {
            ex.printStackTrace();
            Logger.getLogger(Processamento.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } catch (IOException ex) {
            ex.printStackTrace();
            Logger.getLogger(Processamento.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } catch(Exception ex){
            ex.printStackTrace();
            return null;
        }
        
    }
    
}
