/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package constantes;

/**
 *
 * @author Paulo Jr
 */
public class tabelas {
    
    public static class login{
        public static final String TABELA = "Login";
        public static final String ID = "idLogin";
        public static final String LOGIN = "Email";
        public static final String SENHA = "Senha";
        public static final String USUARIO_IDUSUARIO = "Usuario_idUsuario";
    }
    
    public static class usuario{
        public static final String TABELA = "Usuario";
        public static final String ID = "idUsuario";
        public static final String NOME = "Nome";
    }
    
}
