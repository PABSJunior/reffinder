/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package constantes;

/**
 *
 * @author Paulo Jr
 */
public class Constantes {
    
    public static final String RETORNO = "retorno";
    public static final String SIM = "sim";
    public static final String NAO = "nao";
    public static final String MENSAGEM = "mensagem";
    public static final String CONEXAO = "conn";
    public static final String SESSAO = "usuarioLogado";
    
}
