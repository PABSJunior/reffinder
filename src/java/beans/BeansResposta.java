/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

/**
 *
 * @author Paulo Jr
 */
public class BeansResposta {
    
    private String resposta;
    private String thead;
    private String tbody;
    private String tabs;
    private String tabsconteudo;

    /**
     * @return the resposta
     */
    public String getResposta() {
        return resposta;
    }

    /**
     * @param resposta the resposta to set
     */
    public void setResposta(String resposta) {
        this.resposta = resposta;
    }

    /**
     * @return the thead
     */
    public String getThead() {
        return thead;
    }

    /**
     * @param thead the thead to set
     */
    public void setThead(String thead) {
        this.thead = thead;
    }

    /**
     * @return the tbody
     */
    public String getTbody() {
        return tbody;
    }

    /**
     * @param tbody the tbody to set
     */
    public void setTbody(String tbody) {
        this.tbody = tbody;
    }

    /**
     * @return the tabs
     */
    public String getTabs() {
        return tabs;
    }

    /**
     * @param tabs the tabs to set
     */
    public void setTabs(String tabs) {
        this.tabs = tabs;
    }

    /**
     * @return the tabsconteudo
     */
    public String getTabsconteudo() {
        return tabsconteudo;
    }

    /**
     * @param tabsconteudo the tabsconteudo to set
     */
    public void setTabsconteudo(String tabsconteudo) {
        this.tabsconteudo = tabsconteudo;
    }
    
}
