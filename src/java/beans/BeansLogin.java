/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author Paulo Jr
 */
public class BeansLogin {
    
    @NotNull(message="O login não pode ser nulo")
    @NotEmpty(message="O login não pode está vazio")
    @Size(min=4, message="O login deve ter mais de {min} caracteres")
    private String login;
    
    @NotNull(message="A senha não pode ser nulo")
    @NotEmpty(message="A senha não pode está vazio")
    @Size(min=4, message="A senha deve ter mais de {min} caracteres")
    private String senha;

    public BeansLogin() {
    }

    public BeansLogin(String login, String senha) {
        this.login = login;
        this.senha = senha;
    }

    /**
     * @return the login
     */
    public String getLogin() {
        return login;
    }

    /**
     * @param login the login to set
     */
    public void setLogin(String login) {
        this.login = login;
    }

    /**
     * @return the senha
     */
    public String getSenha() {
        return senha;
    }

    /**
     * @param senha the senha to set
     */
    public void setSenha(String senha) {
        this.senha = senha;
    }
    
}
