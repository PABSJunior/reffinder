/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

/**
 *
 * @author Paulo Jr
 */
public class BeansUsuario {
    
    private int id;
    private String nome;

    public BeansUsuario() {
    }

    public BeansUsuario(int id, String nome) {
        this.id = id;
        this.nome = nome;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public boolean equals(Object obj) {
        
        if( this == obj )
            return true;
        else if( obj instanceof BeansUsuario ){
            BeansUsuario user = ((BeansUsuario) obj);
            if( user.getNome().equals(getNome()) )
                return true;
        }
        
        return false;
        
    }

}
