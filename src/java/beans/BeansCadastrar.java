/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author Paulo Jr
 */
public class BeansCadastrar {
    
    @Email
    @NotEmpty
    @NotNull
    private String email;
    
    @NotEmpty
    @NotNull
    private String senha;
    
    @NotEmpty
    @NotNull
    private String confirmarsenha;
    
    @NotEmpty
    @NotNull
    private String sexo;
    
    @NotEmpty
    @NotNull
    private String nome;

    public BeansCadastrar() {
    }

    public BeansCadastrar(String email, String senha, String confirmarsenha, String sexo, String nome) {
        this.email = email;
        this.senha = senha;
        this.confirmarsenha = confirmarsenha;
        this.sexo = sexo;
        this.nome = nome;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the senha
     */
    public String getSenha() {
        return senha;
    }

    /**
     * @param senha the senha to set
     */
    public void setSenha(String senha) {
        this.senha = senha;
    }

    /**
     * @return the confirmarsenha
     */
    public String getConfirmarsenha() {
        return confirmarsenha;
    }

    /**
     * @param confirmarsenha the confirmarsenha to set
     */
    public void setConfirmarsenha(String confirmarsenha) {
        this.confirmarsenha = confirmarsenha;
    }

    /**
     * @return the sexo
     */
    public String getSexo() {
        return sexo;
    }

    /**
     * @param sexo the sexo to set
     */
    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }
    
    public boolean senhaEquals(){
        return senha.equals(confirmarsenha);
    }
    
}
