package conexao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConexaoBD {
    
//    private static final String BANCO = "geo";
    private static final String BANCO = "reffinder";
    private static final String USUARIO = "root";
    private static final String SENHA = "12345";
    
    public Connection getConnection(){
        
        try {
            Class.forName("com.mysql.jdbc.Driver");
            return DriverManager.getConnection("jdbc:mysql://localhost/"+BANCO, USUARIO, SENHA);
        } catch (SQLException | ClassNotFoundException ex) {
            throw new RuntimeException(ex);
        }
        
    }
    
}
