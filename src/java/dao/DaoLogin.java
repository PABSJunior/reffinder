/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import beans.BeansCadastrar;
import beans.BeansUsuario;
import constantes.tabelas;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Paulo Jr
 */
public class DaoLogin {
    
    private Connection conn;
    
    public static final int ERRO = -1, NAOEXISTE = -2, MAISDEUM = -3;
    public static final int ESTALOGADO = 1, NAOESTALOGADO = 0;

    public DaoLogin(Connection conn) {
        this.conn = conn;
    }
    
    public Object logar(String Login, String Senha){
        
        String tabelalogin = tabelas.login.TABELA;
        String tabelausuario = tabelas.usuario.TABELA;
        
        String idusuario = tabelas.usuario.ID;
        String nomeusuario = tabelas.usuario.NOME;
        
        String login = tabelas.login.LOGIN;
        String senha = tabelas.login.SENHA;
        String usuario_idusuario = tabelas.login.USUARIO_IDUSUARIO;
        
        String sql = "SELECT * FROM "+tabelalogin
                + " INNER JOIN "+tabelausuario+" ON "+tabelausuario+"."+idusuario+"="+tabelalogin+"."+usuario_idusuario
                + " WHERE "+login+"=? AND "+senha+"=md5(?)";
        
        try(
                PreparedStatement stmt = conn.prepareStatement(sql);
            ){
            
            stmt.setString(1, Login);
            stmt.setString(2, Senha);
            
            try(
                    ResultSet rs = stmt.executeQuery();
                ){
                
                if( rs.getRow() > 1 )
                    return MAISDEUM;
                
                if( rs.first() ){
                    
                    BeansUsuario b = new BeansUsuario();
                    b.setId(rs.getInt(idusuario));
                    b.setNome(rs.getString(nomeusuario));
                    
                    return b;
                    
                }else
                    return NAOEXISTE;
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(DaoLogin.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
            return ERRO;
        }
        
    }
    
    public int cadastrar(BeansCadastrar cadastro){
        
        String sql = "CALL cadastrar(?, ?, ?, ?)";
        
        try( PreparedStatement stmt = conn.prepareStatement(sql) ){
            
            stmt.setString(1, cadastro.getNome());
            stmt.setString(2, cadastro.getEmail());
            stmt.setString(3, cadastro.getSenha());
            stmt.setBoolean(4, cadastro.getSexo().toUpperCase().startsWith("M"));
            
            ResultSet rs = stmt.executeQuery();
            
            int resultado = -1;
            
            if( rs.first() ){
                resultado = Integer.parseInt(rs.getString("resposta"));
                System.out.println("CADASTRAR: retornou do banco de dados: "+resultado);
            }else
                System.out.println("CADASTRAR: não houve primeiro no banco de dados");
            
            return resultado;
            
        } catch (SQLException ex) {
            Logger.getLogger(DaoLogin.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
            return ERRO;
        }
        
    }
    
}
