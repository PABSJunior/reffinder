/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author Paulo Jr
 */
@Controller
public class ControllerLogado {
    
    private static final String PADRAO = "logado";
    
    @RequestMapping("Logado")
    public String logado(){
        
        return PADRAO+"/index";
        
    }
    
    @RequestMapping("Logado/Sair")
    public String sair(HttpSession session){
        
        try{
            session.invalidate();
        }catch(IllegalStateException e){
            e.printStackTrace();
        }
        
        return "redirect:/";
        
    }
    
}
