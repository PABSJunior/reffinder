/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import beans.BeansCadastrar;
import beans.BeansLogin;
import beans.BeansResposta;
import beans.BeansUsuario;
import constantes.Constantes;
import dados.Processamento;
import dao.DaoLogin;
import java.sql.Connection;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author Paulo Jr
 */
@Controller
public class ControllerPrincipal {
    
    @RequestMapping("/")
    public String index(){
        return "index";
    }
    
    @RequestMapping("Logando")
    public String logando(BeansLogin dados, BindingResult resultado, HttpServletRequest request){
        
        String pagina = "ajax/retornaMensagem";
        
        try{
            
            if( resultado.hasErrors() ){
                request.setAttribute(Constantes.RETORNO, Constantes.NAO);
                request.setAttribute(Constantes.MENSAGEM, "Preencha os dados corretamente");
            }else if( dados.getLogin().contains(" ") ){
                request.setAttribute(Constantes.RETORNO, Constantes.NAO);
                request.setAttribute(Constantes.MENSAGEM, "O login não pode conter espaços");
            }else{
                Connection conn = (Connection) request.getAttribute(Constantes.CONEXAO);
                DaoLogin dao = new DaoLogin(conn);
                
                Object usuario = dao.logar(dados.getLogin(), dados.getSenha());
                
                if( usuario instanceof Integer ){
                    request.setAttribute(Constantes.RETORNO, Constantes.NAO);
                    request.setAttribute(Constantes.MENSAGEM, "Login e/ou senhas estão incorretos");
                }else{
                    request.setAttribute(Constantes.RETORNO, Constantes.SIM);
                    HttpSession session = request.getSession();
                    session.setAttribute(Constantes.SESSAO, (BeansUsuario) usuario);
                }
            }
            
        }catch(Exception e){
            request.setAttribute(Constantes.RETORNO, Constantes.NAO);
            request.setAttribute(Constantes.MENSAGEM, "Ocorreu um erro incomum");
            e.printStackTrace();
        }
        
        return pagina;
        
    }
    
    @RequestMapping("Cadastrar")
    public String cadastrar(BeansCadastrar cadastro, BindingResult resultado, HttpServletRequest request){
        
        String pagina = "ajax/retornaMensagem";
        
        if( resultado.hasErrors() ){
            request.setAttribute(Constantes.RETORNO, Constantes.NAO);
            request.setAttribute(Constantes.MENSAGEM, "Preencha os campos corretamente");
        }else if( !cadastro.senhaEquals() ){
            request.setAttribute(Constantes.RETORNO, Constantes.NAO);
            request.setAttribute(Constantes.MENSAGEM, "As senhas não se correspondem");
        }else{
            
            Connection conn = (Connection) request.getAttribute(Constantes.CONEXAO);
            DaoLogin dao = new DaoLogin(conn);
            
            if( dao.cadastrar(cadastro) > 0 ){
                request.setAttribute(Constantes.RETORNO, Constantes.SIM);
                request.setAttribute(Constantes.MENSAGEM, "Cadastrado com sucesso");
            }else{
                request.setAttribute(Constantes.RETORNO, Constantes.NAO);
                request.setAttribute(Constantes.MENSAGEM, "Ocorreu um erro ao realizar o cadastro");
            }
            
        }
        
        return pagina;
        
    }
    
    @RequestMapping("Dados")
    public String dados(String dados, HttpServletRequest request){
        
        String pagina = "ajax/retornaDados";
        
        dados = dados == null ? "" : dados;
        
        if( dados.trim().isEmpty() ){
            request.setAttribute(Constantes.RETORNO, Constantes.NAO);
            request.setAttribute(Constantes.MENSAGEM, "Preencha os dados corretamente");
        }else{
            
            BeansResposta resposta = new Processamento().processa(dados);
            
            if( resposta == null ){
                request.setAttribute(Constantes.RETORNO, Constantes.NAO);
                request.setAttribute(Constantes.MENSAGEM, "Ocorreu um erro desconhecido");
            }else{
                request.setAttribute(Constantes.RETORNO, Constantes.SIM);
                request.setAttribute(Constantes.MENSAGEM, resposta);
            }
            
        }
        
        return pagina;
        
    }
    
}
