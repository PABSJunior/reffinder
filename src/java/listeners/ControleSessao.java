/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package listeners;

import conexao.ConexaoBD;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 *
 * @author Paulo Júnior
 */
public class ControleSessao implements HttpSessionListener{

    @Override
    public void sessionCreated(HttpSessionEvent se) {
//        System.err.println("Criou sessao: "+se);
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent se) {
        
//        HttpSession sessao = se.getSession();
//        BeansUsuario usuario = (BeansUsuario) sessao.getAttribute(Constantes.SESSAO);
//        String nomeUsuario = usuario.getNome();
//        
        Connection conn = new ConexaoBD().getConnection();
//        DaoLogin dao = new DaoLogin(conn);
//        int valor = dao.controlaLogado(nomeUsuario, DaoLogin.NAOESTALOGADO);
        
//        Locale locale = new Locale("pt", "BR");
//        Date date = Calendar.getInstance(locale).getTime();
//        String data = new SimpleDateFormat("dd 'de' MMMM 'de' yyyy - HH:mm", locale).format(date);
//        System.out.println("Data: \""+data+"\" - Usuario: \""+nomeUsuario+"\" - deslogado? "+(valor == 1));
        
        try {
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(ControleSessao.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
        
    }
    
}
