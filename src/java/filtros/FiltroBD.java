package filtros;

import conexao.ConexaoBD;
import constantes.Constantes;
import java.sql.Connection;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class FiltroBD extends HandlerInterceptorAdapter{

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        
        Connection conn = new ConexaoBD().getConnection();
        request.setAttribute(Constantes.CONEXAO, conn);
        
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        
        Connection conn = (Connection) request.getAttribute(Constantes.CONEXAO);
        request.removeAttribute(Constantes.CONEXAO);
        conn.close();
        
    }
    
}
