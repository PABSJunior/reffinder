package filtros;

import constantes.Constantes;
import constantes.Paginas;
import java.io.PrintWriter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class FiltroLogar extends HandlerInterceptorAdapter{
    
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        
        String url = request.getRequestURL().toString();
        int logado = url.indexOf("Logado");
        HttpSession session = request.getSession(false);
        
        int css = url.contains(".css") || url.contains("/css/") ? 1 : -1;
        int js = url.indexOf(".js");
        int jpg = url.indexOf(".jpg");
        int jpeg = url.indexOf(".jpeg");
        int png = url.indexOf(".png");
//        int gif = url.indexOf(".gif");
        
        if( css != -1 || js != -1 || jpg != -1 || jpeg != -1 || png != -1/* || gif != -1*/ ) return true;
        
        //verifica se existe a palavra "logado" na url, se não existir, logo, entrará neste if
        if(logado == -1){
            
            //verifica se existe uma sessão
            if( session != null ){
                
                if( session.getAttribute(Constantes.SESSAO) != null ){

                    //se tiver sessão ele vai para a página das pessoas que já logaram
                    response.sendRedirect(request.getContextPath()+"/Logado");

                    return false;

                }
                
                session.invalidate();
                
            }
            
            //se não tiver sessão. Pode continuar
            return true;
            
        }
        
        //se existir a palavra "logado" na url
        
        //se existe uma sessão
        if( session != null ){
            
            if( session.getAttribute(Constantes.SESSAO) != null )
                return true;
            
            session.invalidate();
            
        }
        
        //se não existe uma sessão, então, vá logar-se
        PrintWriter out = response.getWriter();
        out.printf(Paginas.erroAoForcaLogar, request.getContextPath());
        
        //response.sendRedirect("erro");
        return false;
        
    }
    
}
