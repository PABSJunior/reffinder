$(document).ready(function(){
    
    /*$("#formulario_logar").validate({
        errorElement: "div",
        validClass: "is-valid",
        errorClass: "is-invalid",
        rules: {
            login: {
                required: true,
                minlength: 4
            },
            senha: {
                required: true,
                minlength: 4
            }
        },
        messages: {
            login: {
                required: "Digite o login",
                minlength: "O login deve conter, no mínimo, {0} caracteres"
            },
            senha: {
                required: "Digite a senha",
                minlength: "A senha deve conter, no mínimo, {0} caracteres"
            }
        }, errorPlacement: function(error, element){
            
            element.addClass("is-invalid");
            error.addClass("invalid-feedback");
            
            error.appendTo( element.parent() );
            
        }, submitHandler: function (form) {
            
            $("#carregando").hide("slow");
            $("#erro").hide("slow");
            $("#sucesso").hide("slow");
            
            $.ajax({
                method: "POST",
                url: "Logando",
                data: $("#formulario_logar").serialize(),
                beforeSend: function () {
                    $("#erro").hide("slow");
                    $("#sucesso").hide("slow");
                    $("#carregando").show("slow");
                },error: function(){
                    $("#erro").html("Ocorreu um erro");
                    $("#sucesso").hide("slow");
                    $("#carregando").hide("slow");
                    $("#erro").show("slow");
                }, success: function (html) {
                    
                    logado = html.indexOf("container") > -1;
                    
                    if (html.trim() == "true" || logado) {
                        $("#carregando").hide("slow");
                        $("#sucesso").show("slow");
                        //location.reload();
                        location.replace("Logado");
                    } else {
                        $("#erro").html(html);
                        $("#carregando").hide("slow");
                        $("#sucesso").hide("slow");
                        $("#erro").show("slow");
                    }
                    
                }
            });
            
            return false;
            
        }
        
    });*/
    
    $("#formulario_cadastrar").validate({
        errorElement: "div",
        validClass: "is-valid",
        errorClass: "is-invalid",
        rules: {
            email: {
                required: true,
                minlength: 4,
                email: true
            },
            senha: {
                required: true,
                minlength: 4
            },
            confirmarsenha: {
                required: true,
                minlength: 4,
                equalTo: senha
            },
            nome: "required",
            sexo: "required"
        },
        messages: {
            email: {
                required: "Digite o email",
                email: "Digite um email válido",
                minlength: "O email deve conter, no mínimo, {0} caracteres"
            },
            senha: {
                required: "Digite a senha",
                minlength: "A senha deve conter, no mínimo, {0} caracteres"
            },
            confirmarsenha: {
                required: "Digite a senha novamente",
                minlength: "A senha deve conter, no mínimo, {0} caracteres",
                equalTo: "As senhas não correspondem"
            },
            nome: "Digite o seu nome completo",
            sexo: "Selecione o seu sexo"
        },
        errorPlacement: function(error, element){
            
            if(element.is(":radio")){
                error.insertAfter(element.parent().parent());
                error.addClass("invalid-feedback mb-3");
                error.css("margin-top", "-3%");
            }else{
                
                element.addClass("is-invalid");
                error.addClass("invalid-feedback");
                
                error.appendTo( element.parent() );
                
            }
            
        }, submitHandler: function (form) {
            
            $("#carregandoCadastrar").hide("slow");
            $("#erroCadastrar").hide("slow");
            $("#sucessoCadastrar").hide("slow");
            
            $.ajax({
                method: "POST",
                url: "Cadastrar",
                data: $("#formulario_cadastrar").serialize(),
                beforeSend: function () {
                    $("#erroCadastrar").hide("slow");
                    $("#sucessoCadastrar").hide("slow");
                    $("#carregandoCadastrar").show("slow");
                }, error: function(){
                    $("#erroCadastrar").html("Ocorreu um erro");
                    $("#sucessoCadastrar").hide("slow");
                    $("#carregandoCadastrar").hide("slow");
                    $("#erroCadastrar").show("slow");
                }, success: function (html) {
                    if (html.trim() == "true") {
                        $("#sucessoCadastrar").html("Cadastrado com sucesso");
                        $("#carregandoCadastrar").hide("slow");
                        $("#erroCadastrar").hide("slow");
                        $("#sucessoCadastrar").show("slow");
                    } else {
                        $("#erroCadastrar").html(html);
                        $("#carregandoCadastrar").hide("slow");
                        $("#sucessoCadastrar").hide("slow");
                        $("#erroCadastrar").show("slow");
                    }
                    
                }
            });
            
            return false;
            
        }
        
    });
    
    $("#formulario").validate({
        errorElement: "div",
        validClass: "is-valid",
        errorClass: "is-invalid",
        rules: {
            dados: "required"
        },
        messages: {
            dados: "Digite os dados"
        },
        errorPlacement: function(error, element){
            
            element.addClass("is-invalid");
            error.addClass("invalid-feedback");
            
            error.appendTo( element.parent() );
            
        }, submitHandler: function (form) {
            
            $("#carregando").hide("slow");
            $("#erro").hide("slow");
            $("#sucesso").hide("slow");
            $("#resposta").hide("slow").html("");
            
            $.ajax({
                method: "POST",
                url: "Dados",
                data: $("#formulario").serialize(),
                beforeSend: function () {
                    $("#erro").hide("slow");
                    $("#sucesso").hide("slow");
                    $("#carregando").show("slow");
                }, error: function(){
                    $("#erro").html("Ocorreu um erro");
                    $("#sucesso").hide("slow");
                    $("#carregando").hide("slow");
                    $("#erro").show("slow");
                }, success: function (html) {
                    
                    var palavra = "true";
                    var local = html.indexOf(palavra);
                    
                    if ( local > -1 ) {
                        $("#sucesso").html("Sucesso");
                        $("#carregando").hide("slow");
                        $("#erro").hide("slow");
                        $("#sucesso").show("slow");
                        $("#resposta").html(html.substring(local+palavra.length)).show("slow");
                    } else {
                        $("#erro").html(html);
                        $("#carregando").hide("slow");
                        $("#sucesso").hide("slow");
                        $("#erro").show("slow");
                    }
                    
                }
            });
            
            return false;
            
        }
        
    });
    
});