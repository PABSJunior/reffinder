<%-- 
    Document   : index
    Created on : 08/11/2018, 09:42:26
    Author     : Paulo Jr
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.5">
        <title>Testar RefFinder</title>
<!--        <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/imagens/logo.png"/>-->
        <link href="${pageContext.request.contextPath}/css/bootstrap.4.1.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/css/index.css" rel="stylesheet">
        <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.2.0.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.4.1.js"></script>
        <script src="${pageContext.request.contextPath}/js/jquery.validate.min.1.17.0.js"></script>
        <script src="${pageContext.request.contextPath}/js/validaDados.js"></script>
        
        <script type="text/javascript">
            
            $(document).ready(function () {
                
                $("#btnExemplo").on("click keypress", function (event) {
                    
                    var evento = event.type;
                    
                    if (evento == "keypress" && (window.event ? event.keyCode : event.which) != 13)
                        return;
                    
                    var texto = "hBAct	hGAPDH	hSDHA	hTBCA	hTUBA1A	hRNU44	hU6	hRNU48	hRNU47	h18s\n19.3112	22.28325	24.8479	22.9217	24.7194	17.5574	14.46205	19.4794	16.4062	18.99305\n19.16265	22.63935	24.93535	22.8954	24.7734	17.58445	14.4329	19.5376	16.4733	19.33055\n19.14815	22.3895	24.56275	22.51135	24.4619	17.93015	14.4635	19.73925	16.5504	19.5449\n21.81065	24.6102	26.5362	23.36915	27.01725	18.1465	14.4691	20.0296	17.003	19.4468\n21.1704	24.0964	26.02375	23.5005	26.0287	17.7986	15.0001	19.6619	16.43175	19.5778\n23.4701	25.95015	27.0499	24.54845	28.30655	18.60915	16.04265	20.5171	17.3307	20.03305\n19.27045	23.49115	25.0835	22.84805	24.67245	17.7206	14.336	19.8189	16.5204	19.30995\n19.0253	22.8714	24.69045	22.7619	24.47635	17.8875	14.47215	19.87185	16.61655	20.05875\n19.16015	22.9632	24.68925	22.5935	24.49845	18.026	14.72145	19.98605	16.76375	20.56225\n20.23935	24.2292	25.4872	23.1425	25.45795	17.62315	14.73475	19.68395	16.3622	20.12155\n20.6476	23.9726	25.84975	23.4667	25.92005	17.91115	15.0755	19.7871	16.47465	20.0937\n22.8857	26.0722	27.2926	24.5212	27.9778	17.6749	15.2755	19.76915	16.386	20.35435\n19.96615	22.7419	25.27745	22.9304	25.04025	18.04825	14.99655	20.29905	16.9748	20.3836\n20.0786	22.61245	25.4461	22.79935	24.9942	17.74855	14.5316	20.155	16.67935	20.22445\n20.7771	23.82425	25.7362	22.70535	25.11675	16.88815	13.50115	19.1055	15.6059	18.39635\n21.58675	23.7839	26.3449	23.28645	26.0738	18.09565	15.0952	20.4421	17.02225	20.12955\n22.15435	24.16015	26.665	23.533	26.52845	17.21855	14.51215	19.70135	16.02825	18.68725\n24.07285	26.44245	27.4036	24.6452	29.01625	18.28	15.592	20.3794	16.7971	20.24645";
                    
                    $("textarea").val(texto);
                    
                    return false;
                    
                });
                
            });
            
        </script>
        
    </head>
    <body style="background-color: #f5f5f5">
        
        <%-- Modal --%>
        <div class="modal fade" id="Modal" tabindex="-1" role="dialog" aria-labelledby="modalT" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modalCadastrar">Cadastre-se</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        
                        <form id="formulario_cadastrar" method="POST">
                            
                            <div class="form-label-group">
                                <input type="email" id="email" name="email" class="form-control" placeholder="E-mail" required autofocus />
                                <label class="labelNormal" for="email">E-mail</label>
                            </div>
                            
                            <div class="form-label-group">
                                <input type="password" name="senha" id="senha" class="form-control" placeholder="Senha" required />
                                <label class="labelNormal" for="senha">Senha</label>
                            </div>
                            
                            <div class="form-label-group">
                                <input type="password" name="confirmarsenha" id="confirmarsenha" class="form-control" placeholder="Confime sua senha" required />
                                <label class="labelNormal" for="confirmarsenha">Confirme sua senha</label>
                            </div>
                            
                            <div class="form-label-group">
                                <input type="text" id="nome" name="nome" class="form-control" placeholder="Nome Completo" required />
                                <label class="labelNormal" for="nome">Nome Completo</label>
                            </div>
                            
                            <div class="mb-3">
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input class="custom-control-input" type="radio" name="sexo" id="a" value="masculino" required />
                                    <label class="custom-control-label" for="a">Masculino</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                    <input class="custom-control-input" type="radio" name="sexo" id="b" value="feminino" required />
                                    <label class="custom-control-label" for="b">Feminino</label>
                                </div>
                                <%--<div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="sexo" id="a" value="masculino" required />
                                    <label class="form-check-label" for="a">Masculino</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="sexo" id="b" value="feminino" required />
                                    <label class="form-check-label" for="b">Feminino</label>
                                </div>--%>
                            </div>
                            
                            <div id="carregandoCadastrar" class="alert alert-info inv mb-2" role="alert">Processando...</div>
                            <div id="erroCadastrar" class="alert alert-danger inv mb-2" role="alert">Erro</div>
                            <div id="sucessoCadastrar" class="alert alert-success inv mb-2" role="alert">Sucesso</div>
                            <button type="submit" class="btn btn-lg btn-primary btn-block">Cadastrar</button>
                            
                        </form>
                        
                    </div>
                    <%--<div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Cadastrar btn</button>
                    </div>--%>
                </div>
            </div>
        </div>
        
        <header class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
            
            <div class="navbar-brand">Projeto RefFinder</div>
            
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#items" aria-controls="items" aria-expanded="false" aria-label="Alterna navegação">
                <span class="navbar-toggler-icon"></span>
            </button>
            
            <div class="collapse navbar-collapse navbar-expand" id="items">
                <ul class="navbar-nav flex-row ml-md-auto d-md-flex">
                    <li class="nav-item active">
                        <a class="nav-link" id="home">Home<%-- <span class="sr-only">(current)</span>--%></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link arrumaLink" id="cadastrar" tabindex="0" data-toggle="modal" data-target="#Modal">Cadastre-se</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link arrumaLink disabled" title="Em manutenção" style="no-drop" id="login" tabindex="0" data-toggle="modal" data-target="#Modal">Login</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link arrumaLink" href="${pageContext.request.contextPath}/Logado/Sair" tabindex="0" id="sair">Sair</a>
                    </li>
                </ul>
            </div>
        </header>
        
        <div class="container" style="margin-top: 5rem !important">
            
            <div class="row">
                
                <form id="formulario" class="formulario" method="POST">
                    
                    <div class="form-label-group">
                        <textarea id="dados" name="dados" class="form-control" placeholder="Dados" required autofocus rows="5" ></textarea>
                        <label class="labelArea" for="dados">Dados</label>
                    </div>
                    
                    <div id="carregando" class="alert alert-info inv mb-2" role="alert">Processando...</div>
                    <div id="erro" class="alert alert-danger inv mb-2" role="alert">Erro</div>
                    <div id="sucesso" class="alert alert-success inv mb-2" role="alert">Sucesso</div>
                    
                    <button id="btnExemplo" class="btn btn-lg btn-secondary metade">Exemplo</button>
                    <button class="btn btn-lg btn-primary metade" type="submit">Enviar</button>
                    
                </form>
                
            </div>
            
            <div id="resposta" class="row"></div>
            
        </div>
        
    </body>
</html>
