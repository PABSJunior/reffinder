<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.5">
        <title>Testar RefFinder</title>
<!--        <link rel="icon" type="image/png" href="${pageContext.request.contextPath}/imagens/logo.png"/>-->
        <link href="${pageContext.request.contextPath}/css/bootstrap.4.1.css" rel="stylesheet">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/login.css" />
        <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.2.0.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.4.1.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.validate.min.1.17.0.js"></script>
        <script src="${pageContext.request.contextPath}/js/validaDados.js"></script>
        <script type="text/javascript">
            
            $(document).ready(function(){
                
                var email = "input[data-name='loginForm']";
                var senha = "input[data-name='senhaForm']";
                
                $(email).on("focus", function(){
                    $(this).parent().css("z-index", "3");
                    $(senha).parent().css("z-index", "0");
                });
                
                $(senha).on("focus", function(){
                    $(this).parent().css("z-index", "3");
                    $(email).parent().css("z-index", "0");
                });
                
            });
            
        </script>
    </head>
    
    <body>
        <div id="div_login">
            
            <form id="formulario_logar" class="form-signin" method="POST">
                <h1 class="h3 mb-3 font-weight-normal text-center">Faça login</h1>
                
                <div class="form-label-group loginEmail">
                    <input type="text" id="login" name="login" class="form-control" data-name="loginForm" placeholder="Login" required autofocus />
                    <label for="login">Login</label>
                </div>
                
                <div class="form-label-group loginSenha">
                    <input type="password" name="senha" id="senha" class="form-control" data-name="senhaForm" placeholder="Senha" required />
                    <label for="senha">Senha</label>
                </div>
                
                <div id="carregando" class="alert alert-info inv mb-2" role="alert">Processando...</div>
                <div id="erro" class="alert alert-danger inv mb-2" role="alert">Erro</div>
                <div id="sucesso" class="alert alert-success inv mb-2" role="alert">Sucesso</div>
                
                <button class="btn btn-lg btn-primary btn-block" type="submit">Entrar</button>
            </form>
            
        </div>
    </body>
</html>
