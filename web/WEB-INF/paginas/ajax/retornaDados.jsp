<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<c:if test="${retorno eq 'sim'}">
    true
    <%--<link rel="stylesheet" href="css/themes/base/jquery.ui.all.css">
    <script src="js/ui/jquery.ui.core.js"></script>
    <script src="js/ui/jquery.ui.widget.js"></script>
    <script src="js/ui/jquery.ui.mouse.js"></script>
    <script src="js/ui/jquery.ui.sortable.js"></script>
    <script src="js/ui/jquery.ui.tabs.js"></script>--%>
    <script src="js/jschart.js"></script>
    <div class="card text-center col-12">
        
        <div class="card-header">
            Ranking Order (Better--Good--Average)
        </div>
        
        <div class="card-body">
            
            <div class="table-responsive">
                
                <table class="table table-hover">
                    
                    <thead class="thead-light">
                        ${mensagem.thead}
                    </thead>
                    
                    <tbody>
                        ${mensagem.tbody}
                    </tbody>
                    
                </table>
                
            </div>
            
        </div>
        
    </div>
    <div class="card text-center mt-3 mb-3 col-12">
        
        <div class="card-header">
            
            <ul class="nav nav-tabs card-header-tabs">
                ${mensagem.tabs}
            </ul>
            
        </div>
        
        <div class="card-body">
            ${mensagem.tabsconteudo}
        </div>
        
    </div>
    <script>
        
        $(document).ready(function(){
            
            function fechaTudo(){
                $("div[id*='tabs-']").each(function(){
                    $(this).addClass("d-none");
                });
            }
            
            $("[data-tabs]").click(function(){
                
                if( !$(this).hasClass("active") ){
                    
                    $("[data-tabs]").removeClass("active");
                    fechaTudo();
                    $(this).addClass("active");
                    $($(this).attr("href")).removeClass("d-none");
                    
                }
                
                return false;
                
            });
            
        });
        
    </script>
</c:if>
<c:if test="${retorno eq 'nao'}">
    ${mensagem}
</c:if>